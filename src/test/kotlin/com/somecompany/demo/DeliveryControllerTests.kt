package com.somecompany.demo

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.stream.IntStream

@SpringBootTest
class DeliveryControllerTests(@Autowired private val deliveryController: DeliveryController) {

    @Test
    fun `should create delivery`() {
        val initialSize = deliveryController.getDeliveries().count().block()!!
        deliveryController.createDelivery(createAddress(1)).block()
        deliveryController.createDelivery(createAddress(2)).block()
        deliveryController.createDelivery(createAddress(3)).block()
        val deliveries = deliveryController.getDeliveries().count().block()!!
        assertThat(deliveries).isEqualTo(initialSize + 3)
    }

    @Test
    fun `should search for delivery by id`() {
        deliveryController.createDelivery(createAddress(1))
        val expectedDelivery = deliveryController.getDeliveries().blockFirst()!!
        val delivery = deliveryController.getDeliveryById(expectedDelivery.id!!).block()
        assertThat(delivery).isEqualToComparingFieldByFieldRecursively(expectedDelivery)
    }

    @Test
    fun shouldAddDeliveryToExistingAddress() {
        val address = createAddress(System.currentTimeMillis().toInt())
        deliveryController.createDelivery(address).block()
        deliveryController.createDelivery(address).block()
        val deliveries = deliveryController.getDeliveryByAddress(address.city).count().block()
        assertThat(deliveries).isEqualTo(2)
    }

    @Test
    fun shouldAddPackageToDelivery() {
        val address = createAddress(System.currentTimeMillis().toInt())
        val deliveryId = deliveryController.createDelivery(address).block()!!
        deliveryController.addPackage(deliveryId, DeliveryPackage("test1")).block()
        deliveryController.addPackage(deliveryId, DeliveryPackage("test2")).block()
        deliveryController.addPackage(deliveryId, DeliveryPackage("test3")).block()
        val delivery = deliveryController.getDeliveryById(deliveryId).block()!!
        assertThat(delivery.deliveryPackages).hasSize(3)
    }

    @Test
    fun shouldFindDeliveryByAddress() {
        val index = System.currentTimeMillis()
        val address = createAddress(index.toInt())
        deliveryController.createDelivery(address).block()
        val deliveryByStreet = deliveryController.getDeliveryByAddress(address.street).blockFirst()
        val deliveryByCity = deliveryController.getDeliveryByAddress(address.city).blockFirst()
        val deliveryByZip = deliveryController.getDeliveryByAddress(address.zipCode).blockFirst()
        val deliveryByCountry = deliveryController.getDeliveryByAddress(address.country).blockFirst()
        val deliveryByState = deliveryController.getDeliveryByAddress(address.state).blockFirst()

        assertThat(deliveryByStreet)
                .isEqualToComparingFieldByFieldRecursively(deliveryByCity)
                .isEqualToComparingFieldByFieldRecursively(deliveryByZip)
                .isEqualToComparingFieldByFieldRecursively(deliveryByCountry)
                .isEqualToComparingFieldByFieldRecursively(deliveryByState)
    }

    @Test
    fun shouldProcess1kDelivery() {
        IntStream.range(0, 10_000)
                .parallel()
                .mapToObj(::createAddress)
                .forEach { deliveryController.createDelivery(it).block() }

        assertThat(deliveryController.getDeliveries().count().block()).isGreaterThanOrEqualTo(10_000)
    }

    fun createAddress(index: Int) = Delivery(
            null,
            "street$index",
            "city$index",
            "state$index",
            "zip$index",
            "country$index",
            "instruction$index"
    )
}
