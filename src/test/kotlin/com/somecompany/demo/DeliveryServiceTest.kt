package com.somecompany.demo

import com.somecompany.demo.service.DeliveryService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class DeliveryServiceTest (@Autowired val deliveryService: DeliveryService){
    @Test
    fun `should create and load delivery`() {
        val deliveryId = deliveryService.createDelivery(
                "s1",
                "s2",
                "s3",
                "s4",
                "s5",
                "s6"
        ).block()!!

        assertThat(deliveryId).isNotBlank()

        val delivery = deliveryService.getDelivery(deliveryId).block()!!
        assertThat(delivery).isNotNull()
    }
}