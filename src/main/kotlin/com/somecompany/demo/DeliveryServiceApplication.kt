package com.somecompany.demo

import com.somecompany.demo.service.DeliveryService
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

@SpringBootApplication(scanBasePackages = ["com.somecompany"])
class DeliveryServiceApplication

fun main(args: Array<String>) = runApplication<DeliveryServiceApplication>(*args).start()

@Configuration
class DeliveryServiceConfiguration {
    @Bean
    fun deliveryService(reactiveMongoTemplate: ReactiveMongoTemplate) = DeliveryService(reactiveMongoTemplate)
}