package com.somecompany.demo

import com.somecompany.demo.service.DeliveryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/delivery", produces = [APPLICATION_JSON_VALUE], consumes = [APPLICATION_JSON_VALUE])
class DeliveryController(@Autowired val deliveryService: DeliveryService) {

    @PostMapping
    fun createDelivery(@RequestBody address: Delivery) = with(address) {
        deliveryService.createDelivery(street, city, state, zipCode, country, specialInstructions)
    }

    @PatchMapping("{id}/package")
    fun addPackage(@PathVariable id: String, @RequestBody aDeliveryPackage: DeliveryPackage) =
            deliveryService.addPackage(id, aDeliveryPackage.tag)

    @GetMapping("{id}")
    fun getDeliveryById(@PathVariable id: String) = deliveryService.getDelivery(id)

    @GetMapping
    fun getDeliveries() = deliveryService.getDeliveries()

    @GetMapping(params = ["address"])
    fun getDeliveryByAddress(@RequestParam(name = "address", required = false) address: String) =
            deliveryService.getDeliveryByAddress(address)
}
