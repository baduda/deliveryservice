package com.somecompany.demo.service

import com.somecompany.demo.Delivery
import com.somecompany.demo.DeliveryPackage
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.*
import reactor.core.publisher.Mono

class DeliveryService(private val template: ReactiveMongoTemplate) {

    fun createDelivery(
            street: String,
            city: String,
            state: String,
            zipCode: String,
            country: String,
            specialInstructions: String
    ) = template.save(Delivery(null, street, city, state, zipCode, country, specialInstructions)).map { it.id!! }

    fun getDeliveries() = template.findAll(Delivery::class.java)

    fun addPackage(deliveryId: String, tag: String) = template.findAndModify(
            Query(Delivery::id isEqualTo deliveryId),
            Update().addToSet("deliveryPackages", DeliveryPackage(tag)),
            Delivery::class.java
    )

    fun getDelivery(id: String) = template.findById(id, Delivery::class.java)

    fun getDeliveryByAddress(address: String) = template.find(
            Query(Criteria().orOperator(
                    Delivery::city isEqualTo address,
                    Delivery::street isEqualTo address,
                    Delivery::country isEqualTo address,
                    Delivery::state isEqualTo address,
                    Delivery::zipCode isEqualTo address
            )),
            Delivery::class.java)
}
