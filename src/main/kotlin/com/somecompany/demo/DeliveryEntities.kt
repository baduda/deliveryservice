package com.somecompany.demo

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document
data class Delivery(@Id var id: String? = null,
                    var street: String = "",
                    var city: String = "",
                    var state: String = "",
                    var zipCode: String = "",
                    var country: String = "",
                    var specialInstructions: String = "",
                    var deliveryPackages: MutableCollection<DeliveryPackage> = mutableListOf()
)

data class DeliveryPackage(var tag: String = "")