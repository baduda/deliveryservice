create table ADDRESS_ENTITY
(
    ID       BIGINT not null primary key,
    CITY     VARCHAR(255),
    COUNTRY  VARCHAR(255),
    STATE    VARCHAR(255),
    STREET   VARCHAR(255),
    ZIP_CODE VARCHAR(255)
);

create table DELIVERY_ENTITY
(
    ID         BIGINT not null primary key,
    ADDRESS_ID BIGINT,
    constraint FK4TQX08P8VIT8IOSLXPXPG2R6L foreign key (ADDRESS_ID) references ADDRESS_ENTITY (ID)
);

create table PACKAGE_ENTITY
(
    ID BIGINT not null primary key,
    TAG VARCHAR(255),
    PACKAGES_ID BIGINT,
    constraint FKTMI3L9IN8RR9RSDASLIE5MV5N foreign key (PACKAGES_ID) references DELIVERY_ENTITY (ID)
);

create sequence HIBERNATE_SEQUENCE;




